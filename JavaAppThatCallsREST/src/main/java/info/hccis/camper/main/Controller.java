package info.hccis.camper.main;

import com.google.gson.Gson;
import info.hccis.camper.model.jpa.Booking;
import info.hccis.camper.util.UtilityRest;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;

public class Controller {

    final public static String MENU = "\nMain Menu \nA) Add\nU) Update\n"
            + "V) View\n"
            + "D) Delete\n"
            + "X) eXit";
    final static Scanner input = new Scanner(System.in);
    private static final String URL_STRING = "http://localhost:8081/teetimebooker/api/BookingService/bookings/";

    public static void main(String[] args) {
        boolean endProgram = false;
        do {
            System.out.println(MENU);
            String choice = input.nextLine();
            Booking booking;
            String url;
            switch (choice.toUpperCase()) {
                case "A":
                    booking = createBooking();
                    url = URL_STRING;
                    System.out.println("Url="+url);
                    UtilityRest.addUsingRest(url, booking);
                    break;
                case "U":
                    booking = createBooking();
                    url = URL_STRING+""+booking.getId();
                    System.out.println("Url="+url);
                    UtilityRest.updateUsingRest(url, booking);
                    break;
                case "D":
                    System.out.println("Enter id to delete");
                    Scanner input = new Scanner(System.in);
                    int id = input.nextInt();
                    input.nextLine();  //burn
                    UtilityRest.deleteUsingRest(URL_STRING, id);
                    break;
                case "V":
                    String jsonReturned = UtilityRest.getJsonFromRest(URL_STRING);
                    //**************************************************************
                    //Based on the json string passed back, loop through each json
                    //object which is a json string in an array of json strings.
                    //*************************************************************
                    JSONArray jsonArray = new JSONArray(jsonReturned);
                    //**************************************************************
                    //For each json object in the array, show the first and last names
                    //**************************************************************
                    System.out.println("Here are the rows");
                    Gson gson = new Gson();
                    for (int currentIndex = 0; currentIndex < jsonArray.length(); currentIndex++) {
                        Booking current = gson.fromJson(jsonArray.getJSONObject(currentIndex).toString(), Booking.class);
                        System.out.println(current.toString());
                    }
                    break;

                case "X":
                    endProgram = true;
                    break;
                default:
                    System.out.println("INVALID OPTION");
            }
        } while (!endProgram);
    }

    /**
     * Create a camper object by passing asking user for input.
     * @return camper
     * @since 20171117
     * @author BJM
     */
    public static Booking createBooking() {
        Booking newBooking = new Booking();

        System.out.println("Enter booking id: (0 for add)");
        newBooking.setId(Integer.parseInt(input.nextLine()));
        
        System.out.println("Enter booker name:");
        newBooking.setName1(input.nextLine());
        
        

        return newBooking;
    }

}
